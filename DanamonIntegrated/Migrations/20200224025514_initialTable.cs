﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DanamonIntegrated.Migrations
{
    public partial class initialTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Status = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    MI = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Manager = table.Column<string>(nullable: true),
                    HR = table.Column<string>(nullable: true),
                    Division = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Jobcode = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Timezone = table.Column<string>(nullable: true),
                    Hiredate = table.Column<string>(nullable: true),
                    EmpId = table.Column<string>(nullable: true),
                    BizPhone = table.Column<string>(nullable: true),
                    CellPhone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Addr1 = table.Column<string>(nullable: true),
                    Addr2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Stated = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    SeatingChart = table.Column<string>(nullable: true),
                    RiviewFreq = table.Column<string>(nullable: true),
                    LastRiviewDate = table.Column<string>(nullable: true),
                    CompanyExitDate = table.Column<string>(nullable: true),
                    Custom01 = table.Column<string>(nullable: true),
                    Custom02 = table.Column<string>(nullable: true),
                    Custom03 = table.Column<string>(nullable: true),
                    Custom04 = table.Column<string>(nullable: true),
                    Custom05 = table.Column<string>(nullable: true),
                    Custom06 = table.Column<string>(nullable: true),
                    Custom07 = table.Column<string>(nullable: true),
                    Custom08 = table.Column<string>(nullable: true),
                    Custom09 = table.Column<string>(nullable: true),
                    Custom10 = table.Column<string>(nullable: true),
                    Custom11 = table.Column<string>(nullable: true),
                    Custom12 = table.Column<string>(nullable: true),
                    Custom13 = table.Column<string>(nullable: true),
                    Custom14 = table.Column<string>(nullable: true),
                    Custom15 = table.Column<string>(nullable: true),
                    MatrixManager = table.Column<string>(nullable: true),
                    DefaultLocale = table.Column<string>(nullable: true),
                    Proxy = table.Column<string>(nullable: true),
                    CustomManager = table.Column<string>(nullable: true),
                    SecondManager = table.Column<string>(nullable: true),
                    OnboardingId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "employee");
        }
    }
}
