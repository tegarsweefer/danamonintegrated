﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DanamonIntegrated.Model
{
    public class UserAddDto
    {
        public string name { set; get; }
        public string email { set; get; }
        public string city { set; get; }
        public string country { set; get; }
        public long gender { set; get; }

        public List<string> department_ids { set; get; }
        public UserAddDto()
        {
            department_ids = new List<string>();
        }
    }
}
