﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DanamonIntegrated.Model
{
    public class ObtainTenantAccessTokenDto
    {
        public string app_id { set; get; }
        public string app_secret { set; get; }
    }
    public class ObtainTenantAccessTokenReturn
    {
        public long code { set; get; }
        public long expire { set; get; }
        public string msg { set; get; }
        public string tenant_access_token { set; get; }
    }
}
