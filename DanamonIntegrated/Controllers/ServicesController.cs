﻿using DanamonIntegrated.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser;
using DanamonIntegrated.Controllers.Utils;
using DanamonIntegrated.EntityFramework;
using DanamonIntegrated.EntityFramework.Entities;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using WinSCP;
namespace DanamonIntegrated.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ServicesController : ControllerBase
    {
        private DanamonIntegratedContext _context;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _openLarkClient;

        public ServicesController(DanamonIntegratedContext context, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _context = context;
            _configuration = configuration;
            _openLarkClient = clientFactory.CreateClient(Helper.OPEN_LARK);
        }
        [HttpGet]
        public async Task<ActionResult> GetAsync()
        {
            try
            {
                CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
                CsvEmployeeMapping csvMapper = new CsvEmployeeMapping();
                CsvParser<Employee> csvParser = new CsvParser<Employee>(csvParserOptions, csvMapper);
                var result = csvParser.ReadFromFile(EnvironmentVariabel.GetSftpLocalPath(_configuration)+"TestData.csv", Encoding.ASCII).ToList();
                result.RemoveAt(0);
                foreach (var details in result)
                {
                    var employee = details.Result;
                    _context.Add(employee);
                }
                _context.SaveChanges();

                return Ok("Success");
            }
            catch (Exception e)
            {
                return BadRequest("Failed to Read CSV Files");
            }
        }

        [HttpGet("TestSFTP")]
        public async Task<ActionResult> TestSFTP()
        {
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = EnvironmentVariabel.GetSftpHostname(_configuration),
                    PortNumber = EnvironmentVariabel.GetSftpPortnumber(_configuration),
                    UserName = EnvironmentVariabel.GetSftpUsername(_configuration),
                    Password = EnvironmentVariabel.GetSftpPassword(_configuration),
                    SshHostKeyFingerprint = EnvironmentVariabel.GetSftpSshHostKeyFingerprint(_configuration)
                };
                using (Session session = new Session())
                {
                    session.Open(sessionOptions);

                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    TransferOperationResult transferResult = session.GetFiles(EnvironmentVariabel.GetSftpServerPath(_configuration), EnvironmentVariabel.GetSftpLocalPath(_configuration), false, transferOptions);
                    transferResult.Check();

                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }
                }
                return Ok("Success");
            }
            catch (Exception e)
            {
                return BadRequest("Failed to Get Recent Ticket");
            }
        }

    }
}
