﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DanamonIntegrated.Controllers.Utils
{
    public static class EnvironmentVariabel
    {
        public static string GetOpenLarkUrl(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("open_lark_url");
        }
        public static string GetAppId(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("app_id");
        }
        public static string GetAppSecret(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("app_secret");
        }
        public static string GetSftpHostname(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_hostname");
        }
        public static int GetSftpPortnumber(IConfiguration Configuration)
        {
            return Configuration.GetValue<int>("sftp_portnumber");
        }
        public static string GetSftpUsername(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_username");
        }
        public static string GetSftpPassword(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_password");
        }
        public static string GetSftpSshHostKeyFingerprint(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_SshHostKeyFingerprint");
        }
        public static string GetSftpServerPath(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_server_path");
        }
        public static string GetSftpLocalPath(IConfiguration Configuration)
        {
            return Configuration.GetValue<string>("sftp_local_path");
        }
    }
}
