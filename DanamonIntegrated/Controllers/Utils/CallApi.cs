﻿using DanamonIntegrated.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DanamonIntegrated.Controllers.Utils
{
    public static class CallApi
    {
        public static async Task<ObtainTenantAccessTokenReturn> ObtainTenantAccessToken(IConfiguration configuration, HttpClient client)
        {
            try
            {
                var model = new ObtainTenantAccessTokenDto();
                model.app_id = EnvironmentVariabel.GetAppId(configuration);
                model.app_secret = EnvironmentVariabel.GetAppSecret(configuration);

                var normalizedBody = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                StringContent content = new StringContent(normalizedBody, UnicodeEncoding.UTF8, "application/json");

                var address = "auth/v3/tenant_access_token/internal/";
                var result = await client.PostAsync(address, content);
                var output = await result.Content.ReadAsAsync<ObtainTenantAccessTokenReturn>();
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static async Task<DefaultMessageDto> LarkAddUser(IConfiguration configuration, HttpClient client, UserAddDto model)
        {
            try
            {
                var TenantAccessToken = await ObtainTenantAccessToken(configuration, client);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TenantAccessToken.tenant_access_token);

                var normalizedBody = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                StringContent content = new StringContent(normalizedBody, UnicodeEncoding.UTF8, "application/json");

                var address = "contact/v1/user/add";
                var result = await client.PostAsync(address, content);

                var output = new DefaultMessageDto();
                if (result.IsSuccessStatusCode)
                {
                    output.status = "OK";
                    output.statusmsg = "Message is Created";
                }
                else
                {
                    output.status = "NOK";
                    output.statusmsg = "Failed to Create a Message";
                }
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
