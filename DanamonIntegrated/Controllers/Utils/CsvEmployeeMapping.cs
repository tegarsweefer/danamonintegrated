﻿using DanamonIntegrated.EntityFramework.Entities;
using DanamonIntegrated.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TinyCsvParser.Mapping;

namespace DanamonIntegrated.Controllers.Utils
{
    public class CsvEmployeeMapping : CsvMapping<Employee>
    {
        public CsvEmployeeMapping() : base()
        {
            MapProperty(0, x => x.Status);
            MapProperty(1, x => x.UserId);
            MapProperty(2, x => x.Username);
            MapProperty(3, x => x.Firstname);
            MapProperty(4, x => x.Lastname);

            MapProperty(5, x => x.MI);
            MapProperty(6, x => x.Gender);
            MapProperty(7, x => x.Email);
            MapProperty(8, x => x.Manager);
            MapProperty(9, x => x.HR);

            MapProperty(10, x => x.Division);
            MapProperty(11, x => x.Department);
            MapProperty(12, x => x.Jobcode);
            MapProperty(13, x => x.Title);
            MapProperty(14, x => x.Location);

            MapProperty(15, x => x.Timezone);
            MapProperty(16, x => x.Hiredate);
            MapProperty(17, x => x.EmpId);
            MapProperty(18, x => x.BizPhone);
            MapProperty(19, x => x.CellPhone);

            MapProperty(20, x => x.Fax);
            MapProperty(21, x => x.Addr1);
            MapProperty(22, x => x.Addr2);
            MapProperty(23, x => x.City);
            MapProperty(24, x => x.Stated);

            MapProperty(25, x => x.Zip);
            MapProperty(26, x => x.Country);
            MapProperty(27, x => x.SeatingChart);
            MapProperty(28, x => x.RiviewFreq);
            MapProperty(29, x => x.LastRiviewDate);

            MapProperty(30, x => x.CompanyExitDate);
            MapProperty(31, x => x.Custom01);
            MapProperty(32, x => x.Custom02);
            MapProperty(33, x => x.Custom03);
            MapProperty(34, x => x.Custom04);

            MapProperty(35, x => x.Custom05);
            MapProperty(36, x => x.Custom06);
            MapProperty(37, x => x.Custom07);
            MapProperty(38, x => x.Custom08);
            MapProperty(39, x => x.Custom09);

            MapProperty(40, x => x.Custom10);
            MapProperty(41, x => x.Custom11);
            MapProperty(42, x => x.Custom12);
            MapProperty(43, x => x.Custom13);
            MapProperty(44, x => x.Custom14);

            MapProperty(45, x => x.Custom15);
            MapProperty(46, x => x.MatrixManager);
            MapProperty(47, x => x.DefaultLocale);
            MapProperty(48, x => x.Proxy);
            MapProperty(49, x => x.CustomManager);

            MapProperty(50, x => x.SecondManager);
            MapProperty(51, x => x.OnboardingId);
        }
    }
}
