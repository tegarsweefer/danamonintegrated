﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DanamonIntegrated.EntityFramework.Entities
{
    [Table("employee")]
    public class Employee
    {
        [Key]
        public int Id { set; get; }
        public string Status { set; get; }
        public string UserId { set; get; }
        public string Username { set; get; }
        public string Firstname { set; get; }

        public string Lastname { set; get; }
        public string MI { set; get; }
        public string Gender { set; get; }
        public string Email { set; get; }
        public string Manager { set; get; }

        public string HR { set; get; }
        public string Division { set; get; }
        public string Department { set; get; }
        public string Jobcode { set; get; }
        public string Title { set; get; }

        public string Location { set; get; }
        public string Timezone { set; get; }
        public string Hiredate { set; get; }
        public string EmpId { set; get; }
        public string BizPhone { set; get; }

        public string CellPhone { set; get; }
        public string Fax { set; get; }
        public string Addr1 { set; get; }
        public string Addr2 { set; get; }
        public string City { set; get; }

        public string Stated { set; get; }
        public string Zip { set; get; }
        public string Country { set; get; }
        public string SeatingChart { set; get; }
        public string RiviewFreq { set; get; }

        public string LastRiviewDate { set; get; }
        public string CompanyExitDate { set; get; }

        public string Custom01 { set; get; }
        public string Custom02 { set; get; }
        public string Custom03 { set; get; }
        public string Custom04 { set; get; }
        public string Custom05 { set; get; }
        
        public string Custom06 { set; get; }
        public string Custom07 { set; get; }
        public string Custom08 { set; get; }
        public string Custom09 { set; get; }
        public string Custom10 { set; get; }

        public string Custom11 { set; get; }
        public string Custom12 { set; get; }
        public string Custom13 { set; get; }
        public string Custom14 { set; get; }
        public string Custom15 { set; get; }

        public string MatrixManager { set; get; }
        public string DefaultLocale { set; get; }
        public string Proxy { set; get; }
        public string CustomManager { set; get; }
        public string SecondManager { set; get; }

        public string OnboardingId { set; get; }
    }
}
